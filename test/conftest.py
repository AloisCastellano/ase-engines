import os
import pytest
import shlex


@pytest.fixture(scope='session')
def config():
    from configparser import ConfigParser
    try:
        configpath = os.environ['ASE_ENGINES_CONFIG']
    except KeyError:
        pytest.skip(
            reason='No configuration, please set $ASE_ENGINE_CONFIG=<file>')

    parser = ConfigParser()
    parser.read(configpath)
    return parser


@pytest.fixture
def aims(config):
    return config['aims']
