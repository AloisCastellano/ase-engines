import os
import pytest
from engines.octopus.octopus import main, groundstate
from ase.calculators.octopus import OctopusProfile


@pytest.fixture(scope='session')
def profile(config):
    # TODO: get binary and pseudopotential paths from configfile
    return OctopusProfile(config['executables']['octopus'])


@pytest.fixture(scope='session')
def octopus_gs(tmp_path_factory, profile):
    directory = tmp_path_factory.mktemp('octopus-gs')
    from ase.build import bulk

    atoms = bulk('Si')
    parameters = dict(
        calculationmode='gs',
        kpointsgrid=[[4, 4, 4]],
        kpointsusesymmetries=True,
        extrastates=1,
        spacing='0.5')

    return groundstate(atoms, parameters, directory, profile)


def test_groundstate(octopus_gs):
    assert (octopus_gs.directory / 'inp').exists()
