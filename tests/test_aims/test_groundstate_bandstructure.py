import pytest
import os
import shutil

import numpy as np

from ase.build import bulk
from ase.io.aims import read_aims_output
from ase.spectrum.band_structure import BandStructure

from engines.aims.aims import groundstate, bandstructure, bandstructure_single_shot
from engines.aims.aims import AimsTemplate
from pathlib import Path


@pytest.fixture
def atoms():
    return bulk("Si")


@pytest.fixture
def parameters():
    return dict(
        xc="pw-lda",
        k_grid=[4, 4, 4],
        species_dir=os.environ.get("AIMS_SPECIES_DIR"),
    )


def pytest_namespace():
    return {"gs": None}


@pytest.mark.dependency()
def test_groundstate(atoms, parameters, tmpdir):
    pytest.gs = groundstate(
        atoms,
        parameters,
        tmpdir,
        ["energy", "forces", "stress"],
    )

    pytest.gs.copy_elsi_restart(Path(f"{tmpdir}/aims_copy/"))
    pytest.gs.copy_elsi_restart(Path(f"{tmpdir}/aims_copy/"), True)

    read_atoms = read_aims_output(f"{tmpdir}/aims.out")

    assert np.sum(np.abs(read_atoms.get_forces().flatten())) < 1e-7
    assert int(np.round(read_atoms.get_potential_energy())) == -15696
    assert np.allclose(
        read_atoms.get_stress(), [0.00337464, 0.00337465, 0.00337465, 0.0, 0.0, 0.0]
    )


@pytest.mark.dependency(depends=["test_groundstate"])
def test_bandstructure(atoms, parameters, tmpdir):
    bandpath = atoms.cell.bandpath(density=5)

    bs = bandstructure(
        pytest.gs,
        bandpath,
        parameters,
        Path(tmpdir) / "bandstructure",
    )

    results = bs.read_aims_bands()
    assert len(bandpath.kpts) == len(results["kpts"])
    maxerr = abs(bandpath.kpts - results["kpts"]).max()
    assert maxerr < 1e-5

    bs = BandStructure(bandpath, results["eigenvalues"])
    bs.write(tmpdir / "bs.json")


def test_bandstructure_single_shot(atoms, parameters, tmpdir, mocker):
    bandpath = atoms.cell.bandpath(density=5)

    bs = bandstructure_single_shot(
        atoms,
        bandpath,
        parameters,
        tmpdir,
    )

    results = bs.read_aims_bands()
    assert len(bandpath.kpts) == len(results["kpts"])
    maxerr = abs(bandpath.kpts - results["kpts"]).max()
    assert maxerr < 1e-5

    bs = BandStructure(bandpath, results["eigenvalues"])
    bs.write(tmpdir / "bs.json")
